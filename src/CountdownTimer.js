import { useState, useEffect } from "react";
import "./CountdownTimer.css";

function CountdownTimer(props) {
  const { initialTime } = props;
  const [hours, setHours] = useState(initialTime.hours);
  const [minutes, setMinutes] = useState(initialTime.minutes);
  const [seconds, setSeconds] = useState(initialTime.seconds);
  const [isRunning, setIsRunning] = useState(false);
  const [intervalId, setIntervalId] = useState(null);

  useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(() => {
        if (hours === 0 && minutes === 0 && seconds === 0) {
          clearInterval(intervalId);
          setIsRunning(false);
        } else {
          if (seconds > 0) {
            setSeconds(seconds - 1);
          } else if (minutes > 0) {
            setMinutes(minutes - 1);
            setSeconds(59);
          } else if (hours > 0) {
            setHours(hours - 1);
            setMinutes(59);
            setSeconds(59);
          }
        }
      }, 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, hours, minutes, seconds]);

  function handleStartStopClick() {
    setIsRunning(!isRunning);
  }

  function resetCountdown() {
    clearInterval(intervalId);
    setIntervalId(null);
    setHours(initialTime.hours);
    setMinutes(initialTime.minutes);
    setSeconds(initialTime.seconds);
  }

  return (
    <div className="CountdownTimer">
      <div>
        <input
          type="number"
          value={hours}
          onChange={(e) => {
            if (e.target.value >= 0 || e.target.value < 60) {
              setHours(parseInt(e.target.value));
            }
          }}
        />
        :
        <input
          type="number"
          value={minutes}
          onChange={(e) => {
            if (e.target.value >= 0) {
              setMinutes(parseInt(e.target.value));
            }
          }}
        />
        :
        <input
          type="number"
          value={seconds}
          onChange={(e) => {
            if (e.target.value >= 0) {
              setSeconds(parseInt(e.target.value));
            }
          }}
        />
      </div>

      <button onClick={handleStartStopClick}>
        {isRunning ? "Stop" : "Start"}
      </button>
      <button onClick={resetCountdown}>Reset</button>
    </div>
  );
}

export default CountdownTimer;
