import React from "react";
import CountdownTimer from "./CountdownTimer";

function App() {
  return <CountdownTimer initialTime={{ hours: 0, minutes: 0, seconds: 0 }} />;
}

export default App;
